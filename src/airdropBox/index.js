/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/anchor-has-content */
import React, { useEffect, useState } from "react";
import "./airdropBox.css";
import "./custStyle.css";
import TelegramIcon from "@material-ui/icons/Telegram";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import logo from "../assets/bgs.jpg";
import DiscordIcon from "../assets/DiscordIcon";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
const AirdropBox = () => {
  var endDate = new Date(); // Now
  endDate.setDate(endDate.getDate() + 3);
  const year = new Date().getFullYear();
  const difference = +new Date(`${year}-05-15`) - +new Date();

  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    const id = setTimeout(() => {
      if (difference > 0) {
        setDays(Math.floor(difference / (1000 * 60 * 60 * 24)));
        setHours(Math.floor((difference / (1000 * 60 * 60)) % 24));
        setMinutes(Math.floor((difference / 1000 / 60) % 60));
        setSeconds(Math.floor((difference / 1000) % 60));
      }
    }, 1000);

    return () => {
      clearTimeout(id);
    };
  });
  return (
    <div className="airdropBox-main-wrapper">
      <div className="airdropBox-inner-wrapper">
        <div className="header-wrap">
          <a
            href="https://www.moneyheisttoken.finance/"
            target="_blank"
            className="navbar-wrapper"
          >
            <img src={logo} alt="logo" />
            <h4>Smart Money Coin</h4>
          </a>
          <div className="connet-btn">Connect Wallet</div>
        </div>
        <div className="xena-token-box">
          <div className="xena-token-box-inner-wrapper">
            <div className="content-wrapper">
              <h3 className="heading">Smart Money Coin BOUNTY OFFER</h3>
              {/* <div className="dropdown-manu-wrapper">
                <p>Refferal bonus 500MNT Smart Money Coin</p>
                <span>
                  <KeyboardArrowDownIcon
                    style={{ fontSize: "20px", color: "white" }}
                  />
                </span>
              </div> */}
              <p style={{ marginBottom: "15px", color: "#fff" }}>
                Offer Start April 15, 2022 - May 15, 2022
              </p>
              <div className="calander-main-wrapper">
                <div className="date-wrapper">
                  <span>
                    <h5>{days}</h5>
                    <p>Days</p>
                  </span>
                  <span>
                    <h5>{hours}</h5>
                    <p>Hours</p>
                  </span>
                  <span>
                    <h5>{minutes}</h5>
                    <p>Minutes</p>
                  </span>
                  <span>
                    <h5>{seconds}</h5>
                    <p>Secounds</p>
                  </span>
                </div>
              </div>
            </div>
            <div className="butten-wrapper">
              <h6>Claim AirDrop</h6>
            </div>
            <p className="paragraph">
              <ul>
                <li>+ Follow all Social Media and Claim 1,000$ Reward</li>
                <li>
                  + Invite and Earn 500$when your invite claim bounty reward.
                </li>
              </ul>
            </p>
            <div className="social-icons-wrapper">
              <a href="https://t.me/moneyheisttokenbsc" target="_blank">
                <TelegramIcon style={{ cursor: "pointer" }} />
              </a>
              &nbsp;&nbsp;
              <a href="https://twitter.com/heist_token" target="_blank">
                <TwitterIcon style={{ cursor: "pointer" }} />
              </a>
              &nbsp;&nbsp;
              <a
                href="https://www.facebook.com/MoneyHeistToken/"
                target="_blank"
              >
                <FacebookIcon style={{ cursor: "pointer" }} />
              </a>
              &nbsp;&nbsp;
              <a href="https://discord.com/invite/pTFTyTNgfx" target="_blank">
                <DiscordIcon style={{ cursor: "pointer" }} />
              </a>
              &nbsp;&nbsp;
              <a
                href="https://www.youtube.com/watch?v=1YGoE5Tg8i4"
                target="_blank"
              >
                <YouTubeIcon style={{ cursor: "pointer" }} />
              </a>
            </div>
          </div>
        </div>
        <br />
        {/* =-========== */}
        <section className="app section">
          <div
            className="app-container container flex flex--column"
            style={{ marginTop: "5px!important" }}
          >
            <div
              style={{ textAlign: "center" }}
              className="app__data-container flex flex--column"
            >
              <h2 className="app__title" style={{ color: "#fff" }}>
                Referral program
              </h2>
              <span>
                Share your referral link and get paid instantly to your wallet
                for every referred token claim.
              </span>
              <br />
              <span style={{ overflow: "hidden" }}>
                <br />
                Your Referrer: <span id="referrer">None</span>
              </span>
              <br />
              <div className="app__data-content">
                <p>
                  Total paid to referrers: <span id="refTotal">0</span>
                </p>
                <br />
                <p>
                  Referral commission: <span id="refPercent">$500</span>
                  &nbsp;
                </p>
                <br />
                <p>
                  Your referral earnings: <span id="refMy">0</span>
                </p>
                <br />
                <p>
                  Share your referral link or QR code and get commission for
                  referred token claims instantly to your wallet.
                </p>
                <br />
                <p>
                  <input
                    type="text"
                    id="referLink"
                    defaultValue="Please connect your wallet first (Metamask/Trustwallet)"
                    readOnly="true"
                    style={{
                      width: "90%",
                      borderRadius: "10px",
                      textAlign: "center",
                      border: "none",
                    }}
                  />
                </p>
                {/* <button className="btn" id="copyreflink">
                  Copy link
                </button> */}
                <br />
                <div className="butten-wrapper">
                  <h6>Copy link</h6>
                </div>
                <p id="refErr" className="err" style={{ display: "none" }}>
                  Please connect your wallet on Binance Smart Chain to generate
                  your referral link!
                </p>
              </div>
            </div>
          </div>
        </section>
        <br />
        <br />
      </div>
    </div>
  );
};

export default AirdropBox;
